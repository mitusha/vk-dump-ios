//
//  ViewController.swift
//  VkDump
//
//  Created by Dmitry Salnikov on 16.08.17.
//  Copyright © 2017 mitusha. All rights reserved.
//

import UIKit
import VK_ios_sdk

class ViewController: UIViewController, VKSdkUIDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Operator.instance.vkSdk.uiDelegate = self
        Operator.instance.getAuthorized()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    /**
     Calls when user must perform captcha-check.
     If you implementing this method by yourself, call -[VKError answerCaptcha:] method for captchaError with user entered answer.
     
     @param captchaError error returned from API. You can load captcha image from <b>captchaImg</b> property.
     */
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        print("vkSdkNeedCaptchaEnter")
    }
    
    /**
     Pass view controller that should be presented to user. Usually, it's an authorization window.
     
     @param controller view controller that must be shown to user
     */
    @available(iOS 2.0, *)
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.present(controller, animated: true, completion: nil)
    }
    

}

