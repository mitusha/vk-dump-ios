//
//  Operator.swift
//  VkDump
//
//  Created by Dmitry Salnikov on 16.08.17.
//  Copyright © 2017 mitusha. All rights reserved.
//

import Foundation
import Alamofire
import VK_ios_sdk

class Operator: NSObject, VKSdkDelegate {
    static let instance = Operator()
    override private init(){
        super.init()
        vkSdk.register(self)
    }
    
    let vkSdk = VKSdk.initialize(withAppId: Const.VK.APP_ID)!
    
    func getAuthorized() {
        if (!VKSdk.isLoggedIn()) {
            VKSdk.wakeUpSession(Const.VK.Scope) { (state, error) in
                print("wakeUpSession, state: \(state.rawValue); error: \(String(describing: error))")
                if (state == .authorized) {
                    self.gotAuthorized()
                } else {
                    print("invoke VKSdk.authorize...")
                    VKSdk.authorize(Const.VK.Scope)
                    
                }
            }
        } else {
            print ("already authorized")
            self.gotAuthorized()
        }
    }
    
    /**
     Notifies about authorization was completed, and returns authorization result with new token or error.
     
     @param result contains new token or error, retrieved after VK authorization.
     */
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        print("vkSdkAccessAuthorizationFinished, result: \(String(describing: result))")
        self.gotAuthorized()
    }
    
    /**
     Notifies about access error. For example, this may occurs when user rejected app permissions through VK.com
     */
    func vkSdkUserAuthorizationFailed() {
        print("vkSdkUserAuthorizationFailed")
        
    }
    
    
    /** --------------------------- */
    
    func gotAuthorized() {
        print("got authorized")
        
        dumpWall()
        
    }
    
    // ------------------------------
    
    var wallItemsTotal = 0
    var wallOffset = 0
    let wallPageSize = 100
    
    var items: [Any?] = []
    
    func dumpWall() {
        getWallPagesRecursively {
            print("got all pages!")
            print("retrieved wall items count: \(self.items.count)")
            
            guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
            print("doc dir url: \(documentDirectoryUrl.absoluteString)")
            let filename = "vk-dump-wall-items.json"
            let fileUrl = documentDirectoryUrl.appendingPathComponent(filename)
            let jsonData = try! JSONSerialization.data(withJSONObject: self.items, options: JSONSerialization.WritingOptions.prettyPrinted)
            try! jsonData.write(to: fileUrl)
            print("DONE! json file path: \(fileUrl)")
            
        }
    }
    
    func getWallPagesRecursively(_ completion: @escaping (() -> ())) {
        print("requesting new page, offset: \(self.wallOffset)")
        VKApi.request(withMethod: "wall.get", andParameters: [
            VK_API_OWNER_ID:Const.VK.myPageId,
            VK_API_OFFSET: wallOffset,
            VK_API_COUNT: wallPageSize
            ]).execute(resultBlock: { (response) in
                let jsonNSDict = response!.json!
                
                let dict = jsonNSDict as! [String:AnyObject]
                
                if (self.wallItemsTotal == 0) {
                    self.wallItemsTotal = dict["count"] as! Int
                    print("total items: \(self.wallItemsTotal)")
                }
                
                if let items = dict["items"] as? NSArray as? [Any?] {
                    self.items.append(contentsOf: items)
                }
//                print(dict)
                self.wallOffset += self.wallPageSize
                if (self.wallOffset < self.wallItemsTotal) {
                    self.getWallPagesRecursively(completion)
                } else {
                    completion()
                }
                
            }) { (error) in
                print("[wall get] error:\(String(describing: error))")
        }
    }
    
    // --------------------
    
    func getAudios() {
        VKApi.request(withMethod: "audio.get", andParameters: [VK_API_OWNER_ID:Const.VK.myPageId]).execute(resultBlock: { (response) in
            print("[audio get] response: \(response!.responseString)")
        }) { (error) in
            print("[audio get] error:\(String(describing: error))")
        }
    }
    
    func getFriends() {
        VKApi.friends().get().execute(resultBlock: { (response) in
            //            print("[friends request] response: \(String(describing: response!.json))")
            print(response!.responseString)
        }) { (error) in
            print("[friends request] error: \(String(describing: error))")
        }
    }
    
    
}
